import axios from "axios";


export const http_api = 'http://localhost:5051'
// export const http_api = 'http://10.144.172.214:5151'
export const refetchTimeout = 0.5


export const connectPaper = ({host, port, clientId}) => {
    return axios.post(
        http_api + '/connect_paper', {host, port, clientId}
    ).then((response) => {
        console.log(response.data)
        return response.data
    })
}


export const connectLive = ({host, port, clientId}) => {
    return axios.post(
        http_api + '/connect_live', {host, port, clientId}
    ).then( response => {
        return response.data
    })
}


export const startBackTest = () => {
    return axios.post(
        http_api + '/start_backtest',
    ).then( response => {
        return response.data
    })
}


export const stopLive = () => {
    return axios.post(
        http_api + '/stop_live',
    ).then(response => {
        return response.data
    })
}


export const stopPaper = () => {
    return axios.post(
        http_api + '/stop_paper'
    ).then(response => {
        return response.data
    })
}


export const healthCheckPaper = () => {
    return axios.get(
        http_api + '/status_paper'
    ).then(response => {
        return response.data
    })
}


export const healthCheckLive = () => {
    return axios.get(
        http_api + '/status_live'
    ).then(response => {
        return response.data
    })
}


export const loadPaper = () => {
    return axios.get(
        http_api + '/load_paper'
    ).then(response => {
        return response.data
    })
}


export const loadLive = () => {
    return axios.get(
        http_api + '/load_live'
    ).then(response => {
        return response.data
    })
}
