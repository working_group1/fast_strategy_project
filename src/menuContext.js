import {createContext} from "react";


export const MenuContext = createContext({
    itemsDisabled: false,
    setItemsDisabled: () => {},
});
