import {
    Admin,
    Menu,
    Layout,
    CustomRoutes,
} from 'react-admin';
import {Route} from 'react-router-dom'
import React, {useContext, useState} from "react";
import {dataProvider} from './dataProvider'
import {BackTest, ConnectAccount} from "./components";
import SpeedIcon from '@mui/icons-material/Speed';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import RequestPageIcon from '@mui/icons-material/RequestPage';
import {
    connectPaper,
    connectLive,
    stopLive,
    stopPaper,
    healthCheckLive,
    healthCheckPaper,
    loadLive,
    loadPaper
} from "./Api";
import {MenuContext} from "./menuContext";


const DataMenu = () => {
    const {itemsDisabled} = useContext(MenuContext)

    return (
        <Menu>
            <Menu.Item to={'backtest'} disabled={itemsDisabled} primaryText={'Backtest'} leftIcon={<SpeedIcon/>}/>
            <Menu.Item to={'paper'} disabled={itemsDisabled} primaryText={'Paper'} leftIcon={<NoteAddIcon/>} />
            <Menu.Item to={'live'} disabled={itemsDisabled} primaryText={'Live'} leftIcon={<RequestPageIcon/>} />
        </Menu>
    )
}


const DataLayout = props => (<Layout {...props} menu={DataMenu} />)


const App = () => {
    const [itemsDisabled, setItemsDisabled] = useState(false)

    return (
        <MenuContext.Provider value={{itemsDisabled, setItemsDisabled}}>
            <Admin dataProvider={dataProvider} layout={DataLayout}>
                <CustomRoutes>
                    <Route
                        path="backtest"
                        element={
                            <BackTest/>
                        }
                    />
                    <Route
                        path="paper"
                        element={
                            <ConnectAccount
                                providerName={'paper'}
                                connectFunction={connectPaper}
                                connectionStopFunction={stopPaper}
                                healthCheckFunction={healthCheckPaper}
                                loadFunction={loadPaper}
                            />
                        }
                    />
                    <Route
                        path='live'
                        element={
                            <ConnectAccount
                                providerName={'live'}
                                connectFunction={connectLive}
                                connectionStopFunction={stopLive}
                                healthCheckFunction={healthCheckLive}
                                loadFunction={loadLive}
                            />
                        }
                    />
                </CustomRoutes>
            </Admin>
        </MenuContext.Provider>
    );
}

export default App;
