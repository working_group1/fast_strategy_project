import React from "react";
import {http_api} from './Api'
import JsonDataProvider from 'ra-data-json-server'


export const dataProvider = JsonDataProvider(http_api)
