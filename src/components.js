import
    React,
{useContext, useEffect, useState}
    from 'react';
import {
    List,
    Datagrid,
    TextField,
    TextInput,
    Form,
    Resource,
    Loading,
    useNotify,
    Title
} from 'react-admin';
import Typography from "@mui/material/Typography";
import Paper from '@mui/material/Paper';
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import {refetchTimeout} from "./Api";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import DoNotDisturbOnIcon from '@mui/icons-material/DoNotDisturbOn';
import {useQuery} from 'react-query'
import {
    startBackTest,
} from "./Api";
import {MenuContext} from "./menuContext";


export const BackTest = () => {
    const [data, setData] = useState({})
    const [loading, setLoading] = useState(false)
    const { setItemsDisabled } = useContext(MenuContext);

    const keys = {
        'account_balance': 'Final account balance: ',
        'total_trades': 'Total trades: ',
        'profitable_trades': 'Profitable trades: ',
        'logins_trades': 'Losing trades:'
    }

    const startBacktest = () => {
        setLoading(true)
        setItemsDisabled(true)
        setData({})
        startBackTest().then(data => {
            setData(data)
            setItemsDisabled(false)
            setLoading(false)
        })
    }

    return (
        <>
            <Title title={'backtest'} />
            <Button
                onClick={startBacktest}
                disabled={loading}
            >
                Ran backtest
            </Button>
            {
                loading
                ? <Loading/>
                : null
            }
            {
                Object.keys(data).length !== 0
                    ?
                    <Paper>
                        {
                            Object.keys(data).map(key => (
                                <Typography component={'h6'}>
                                    {`${keys[key]} ${data[key]}`}
                                </Typography>
                            ))
                        }
                    </Paper>
                    : null
            }
        </>
    );
};


const HealthCheckIndicator = ({checkHealthFunction, loadState, healthState, setHealthState}) => {

    const iconByState = {
        'pending': MoreHorizIcon,
        'active': CheckCircleIcon,
        'inactive': DoNotDisturbOnIcon,
    }


    useQuery({
        queryKey: ['status'],
        queryFn: () => {
            if (!loadState) {
                return 'pending'
            }
            return checkHealthFunction().then(data => {setHealthState(data.status)})
        },
        refetchInterval: refetchTimeout * 1000,
        refetchIntervalInBackground: true
    })

    const Icon = iconByState[healthState]

    return (
        <Grid container spacing={2} style={{margin: '20px 0', marginLeft: '20px'}}>
            <Paper>
                <Typography style={{display: 'flex'}}>
                    Current terminal status : {<Icon style={{marginLeft: '15px'}}/>}
                </Typography>
            </Paper>
        </Grid>
    )
}


const ConnectForm = ({setDoLoad, connectFunction, connectionStopFunction, loadFunction, setHealthState, children}) => {

    const [canStop, setCanStop] = useState(false)
    const [host, setHost] = useState('')
    const [port, setPort] = useState('')
    const [clientId, setClientId] = useState('')
    const [loaded, setLoaded] = useState(false)
    const {setItemsDisabled} = useContext(MenuContext)

    const notify = useNotify()

    const handleStart = () => {

        setCanStop(true)
        setItemsDisabled(true)

        connectFunction({host, port, clientId}).then((data) => {

            if (data.status) {
                setDoLoad(true)
            } else {
                notify(data.error, {type: "error"})
                setCanStop(false)
                setItemsDisabled(false)
            }

        })
    }

    const handleStop = () => {
        setCanStop(false)
        setItemsDisabled(false)
        setDoLoad(false)
        setHealthState('pending')
        connectionStopFunction()
    }

    useEffect(() => {
        loadFunction().then(loadedData => {
            setClientId(loadedData.clientId)
            setHost(loadedData.host)
            setPort(loadedData.port)
            setLoaded(true)
        })
    }, [])

    if (!loaded){
        return <Loading/>
    }

    return (
        <Grid item xs={7} style={{margin: '20px 0'}}>
            <Paper style={{paddingBottom: '30px'}}>
                <Grid container spacing={2} style={{marginLeft: '-3px', marginTop: '10px'}}>
                    <Grid item xs={12} >
                        <Form>
                            <TextInput
                                source={'host'}
                                id={'host'}
                                onChange={(event) => {setHost(event.target.value)}}
                                defaultValue={host}
                                style={{margin: '0 5px'}}
                            />
                            <TextInput
                                source={'port'}
                                id={'port'}
                                onChange={(event) => {setPort(event.target.value)}}
                                defaultValue={port}
                                style={{margin: '0 5px'}}
                            />

                            <TextInput
                                source={'clientId'}
                                id={'clientId'}
                                onChange={(event) => {setClientId(event.target.value)}}
                                defaultValue={clientId}
                                style={{margin: '0 5px'}}
                            />
                        </Form>
                    </Grid>
                    {children}
                    <Grid item>
                        <Button
                            variant={'outlined'}
                            style={{margin: '0 20px'}}
                            onClick={handleStart}
                            disabled={canStop}
                        >
                            Start
                        </Button>
                        <Button
                            variant={'outlined'}
                            onClick={handleStop}
                            disabled={!canStop}
                        >
                            Stop
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    )
}


const queryOptions = {
    refetchOnWindowFocus: true,
    refetchOnReconnect: true,
    refetchOnMount: true,
    refetchIntervalInBackground: true,
    refetchInterval: refetchTimeout * 1000,
    retry: false,
}


const TradesList = () => {
    return (
        <List queryOptions={queryOptions}>
            <Datagrid>
                <TextField source={'id'}/>
                <TextField source={'symbol'}/>
                <TextField source={'action'}/>
                <TextField source={'totalQuantity'}/>
                <TextField source={'orderType'}/>
                <TextField source={'price'}/>
                <TextField source={'filledQuantity'}/>
                <TextField source={'orderStatus'}/>
                <TextField source={'filled'}/>
                <TextField source={'remaining'}/>
                <TextField source={'avgFillPrice'}/>
            </Datagrid>
        </List>
    )
}


const PortfolioList = () => {
    return (
        <List queryOptions={queryOptions}>
            <Datagrid>
                <TextField source={'symbol'}/>
                <TextField source={'position'}/>
                <TextField source={'marketPrice'}/>
                <TextField source={'marketValue'}/>
                <TextField source={'averageCost'}/>
                <TextField source={'unrealizedPNL'}/>
                <TextField source={'realizedPNL'}/>
            </Datagrid>
        </List>
    )
}


const OrdersList = () => {
    return (
        <List queryOptions={queryOptions}>
            <Datagrid>
                <TextField source={'symbol'}/>
                <TextField source={'position'}/>
                <TextField source={'marketPrice'}/>
                <TextField source={'marketValue'}/>
                <TextField source={'averageCost'}/>
                <TextField source={'unrealizedPNL'}/>
                <TextField source={'realizedPNL'}/>
            </Datagrid>
        </List>
    )
}


export const ConnectAccount = (
    {
        providerName,
        connectFunction,
        connectionStopFunction,
        healthCheckFunction,
        loadFunction,
    }
    ) => {

    const [doLoad, setDoLoad] = useState(false)
    const [healthState, setHealthState] = useState('pending')

    return (
        <>
            <Title title={providerName} />
            <Grid container spacing={4}>
                <ConnectForm
                    setDoLoad={setDoLoad}
                    connectFunction={connectFunction}
                    connectionStopFunction={connectionStopFunction}
                    loadFunction={loadFunction}
                    setHealthState={setHealthState}
                >
                    <Grid item xs={6}>
                        <HealthCheckIndicator
                            checkHealthFunction={healthCheckFunction}
                            loadState={doLoad}
                            healthState={healthState}
                            setHealthState={setHealthState}
                        />
                    </Grid>
                </ConnectForm>
            </Grid>
            {   doLoad ?
                <div>
                    <Accordion>
                        <AccordionSummary>Portfolio</AccordionSummary>
                        <AccordionDetails>
                            <Resource
                                name={`${providerName}_portfolio`}
                                list={PortfolioList}
                            />
                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary>Trades</AccordionSummary>
                        <AccordionDetails>
                            <Resource
                                name={`${providerName}_trades`}
                                list={TradesList}
                            />
                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary>Orders</AccordionSummary>
                        <AccordionDetails>
                            <Resource
                                name={`${providerName}_orders`}
                                list={OrdersList}
                            />
                        </AccordionDetails>
                    </Accordion>
                </div>
                : null
            }
        </>
    )
}
