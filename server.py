import json
import time
from enum import Enum
from functools import wraps
from typing import Callable, List

from flask import Flask, make_response, Response
from flask_cors import CORS

app = Flask(__name__)
app.config['SECRET_KEY'] = 'your-secret-key'
CORS(app)


class Status(str, Enum):

    PENDING = 'pending'
    INACTIVE = 'inactive'
    ACTIVE = 'active'


def count_writer(function: Callable[[], List[dict]]) -> Callable[[], Response]:
    @wraps(function)
    def count_wrapper() -> Response:
        response = make_response()
        data_list = function()
        response.data = json.dumps(data_list)
        response.headers.update({
            'X-Total-Count': len(data_list),
            'Access-Control-Expose-Headers': 'X-Total-Count'
        })
        return response
    return count_wrapper


@app.route('/connect_paper', methods=['POST'])
def connect_paper() -> None:
    time.sleep(1)
    return {
        'status': True,
        'error': None
    }


@app.route('/connect_live', methods=['POST'])
def connect_live() -> None:
    time.sleep(1)
    return {
        'status': False,
        'error': 'something wrong'
    }


@app.route('/status_live')
def status_live() -> dict:
    return {'status': Status.ACTIVE}


@app.route('/status_paper')
def status_paper() -> dict:
    return {'status': Status.INACTIVE}


@app.route('/stop_paper', methods=['POST'])
def stop_paper() -> None:
    return ''


@app.route('/stop_live', methods=['POST'])
def stop_live() -> None:
    return ''


@app.route('/start_backtest', methods=['POST'])
def start_backtest() -> dict:
    time.sleep(1)
    return {
        'account_balance': 124044,
        'total_trades': 100,
        'profitable_trades': 40,
        'logins_trades': 44
    }


@app.route('/load_paper')
def load_paper() -> dict:
    return {
        'host': '123123',
        'port': 444,
        'clientId': '434'
    }


@app.route('/load_live')
def load_live() -> dict:
    return {
        'host': '123123',
        'port': 444,
        'clientId': '434'
    }


@app.route('/paper_trades')
@count_writer
def paper_trades() -> List[dict]:
    return []


@app.route('/paper_portfolio')
@count_writer
def paper_portfolio() -> List[dict]:
    return []


@app.route('/paper_orders')
@count_writer
def paper_orders() -> List[dict]:
    return []


@app.route('/live_trades')
@count_writer
def live_trades() -> List[dict]:
    return []


@app.route('/live_portfolio')
@count_writer
def live_portfolio() -> List[dict]:
    return []


@app.route('/live_orders')
@count_writer
def live_orders() -> List[dict]:
    return []


if __name__ == '__main__':
    app.run('127.0.0.1', port=5051)
